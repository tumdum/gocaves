package main

import "github.com/nsf/termbox-go"

func setString(x, y int, s string, fg, bg termbox.Attribute) {
	c := 0
	for _, ch := range s {
		termbox.SetCell(x+c, y, ch, fg, bg)
		c++
	}
}

func clear() {
	termbox.Clear(termbox.ColorBlack, termbox.ColorBlack)
}
