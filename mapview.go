package main

import "github.com/nsf/termbox-go"

type MapView struct {
	screenOffset  Position
	width, height int64
}

func (v MapView) setTile(pos Position, tile Tile) {
	globalPos := pos.Add(v.screenOffset)
	termbox.SetCell(int(globalPos.X), int(globalPos.Y),
		tile.Glyph, tile.Fg, tile.Bg)
}

func (v MapView) RenderAt(mapCenter Position, m Map) {
	viewCenter := Position{v.width / 2, v.height / 2}
	mapStart := mapCenter.Sub(viewCenter)
	for y := int64(0); y < v.height; y++ {
		for x := int64(0); x < v.width; x++ {
			viewPos := Position{x, y}
			if viewPos.Equal(viewCenter) {
				v.setTile(viewPos, crosshair)
			} else {
				v.setTile(viewPos, m.At(mapStart.Add(viewPos)))
			}
		}
	}
}
