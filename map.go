package main

import (
	"math/rand"

	"github.com/nsf/termbox-go"
)

var (
	floor     = Tile{'.', termbox.ColorGreen, termbox.ColorBlack}
	wall      = Tile{'#', termbox.ColorYellow, termbox.ColorBlack}
	bounds    = Tile{'X', termbox.ColorBlue, termbox.ColorBlack}
	crosshair = Tile{'X', termbox.ColorRed | termbox.AttrBold, termbox.ColorBlack}
)

type Position struct {
	X, Y int64
}

func (p Position) Up() Position {
	return Position{p.X, p.Y - 1}
}

func (p Position) Down() Position {
	return Position{p.X, p.Y + 1}
}

func (p Position) Left() Position {
	return Position{p.X - 1, p.Y}
}

func (p Position) Right() Position {
	return Position{p.X + 1, p.Y}
}

func (p Position) Add(other Position) Position {
	return Position{p.X + other.X, p.Y + other.Y}
}

func (p Position) Sub(other Position) Position {
	return Position{p.X - other.X, p.Y - other.Y}
}

func (p Position) Equal(other Position) bool {
	return p.X == other.X && p.Y == other.Y
}

type Tile struct {
	Glyph  rune
	Fg, Bg termbox.Attribute
}

func NewMap(x, y int64) Map {
	m := Map{width: x, height: y, tiles: make([][]Tile, y)}
	for i := int64(0); i < y; i++ {
		m.tiles[i] = make([]Tile, x)
	}
	return m
}

type Map struct {
	width, height int64
	tiles         [][]Tile
}

func (m Map) At(pos Position) Tile {
	if pos.X >= 0 && pos.X < m.width && pos.Y >= 0 && pos.Y < m.height {
		return m.tiles[pos.Y][pos.X]
	}
	return bounds
}

func BuildRandomMap(m Map) Map {
	for y := range m.tiles {
		for x := range m.tiles[y] {
			m.tiles[y][x] = randomTile()
		}
	}
	return m
}

func randomTile() Tile {
	if rand.Float32() > 0.42 {
		return floor
	}
	return wall
}

func SmoothMap(oldMap Map) Map {
	newMap := NewMap(oldMap.width, oldMap.height)
	for y := range newMap.tiles {
		for x := range newMap.tiles {
			center := Position{int64(x), int64(y)}
			walls := countWallsAround(center, oldMap)
			if (oldMap.At(center) == wall && walls >= 4) || (oldMap.At(center) != wall && walls >= 5) {
				newMap.tiles[y][x] = wall
			} else {
				newMap.tiles[y][x] = floor
			}
		}
	}
	return newMap
}

func countWallsAround(center Position, m Map) int {
	c := 0
	for y := int64(-1); y < 2; y++ {
		for x := int64(-1); x < 2; x++ {
			if m.At(center.Add(Position{x, y})) == wall {
				c++
			}
		}
	}
	return c
}
