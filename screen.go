package main

import "github.com/nsf/termbox-go"

const (
	winMsg  = "Congratulations, you win!"
	loseMsg = "Sorry, better luck next time."
)

type Screen interface {
	ProcessEvent(ev termbox.Event) (done bool, result interface{})
	Render()
}

type MainScreen struct {
}

func (s MainScreen) Render() {
	clear()
	setString(10, 4, "Welcome to GoCaves!", termbox.ColorWhite|termbox.AttrBold, termbox.ColorBlack)
	setString(10, 6, "Press N to start new game.", termbox.ColorGreen, termbox.ColorBlack)
	setString(10, 7, "Press ESC key to exit.", termbox.ColorRed, termbox.ColorBlack)
}

func (s MainScreen) ProcessEvent(ev termbox.Event) (done bool, result interface{}) {
	if ev.Type == termbox.EventKey {
		if ev.Ch == 'N' || ev.Ch == 'n' {
			w, h := termbox.Size()
			logger.Printf("Terminal size is %d x %d characters.", w, h)
			return false, NewGameScreen(w, h)
		} else if ev.Key == termbox.KeyEsc {
			return true, nil
		}
	}
	return false, nil
}

func NewGameScreen(width, height int) Screen {
	newMap := BuildRandomMap(NewMap(1000, 1000))
	heightOffset := int64(1)
	return &GameScreen{
		m: newMap,
		view: MapView{Position{0, heightOffset},
			int64(width), int64(height) - heightOffset},
	}
}

type GameScreen struct {
	m      Map
	view   MapView
	center Position
}

func (s GameScreen) Render() {
	clear()
	s.view.RenderAt(s.center, s.m)
	setString(0, 0, "Press S to smooth, ←↑→↓ to scrool, ENTER to win, anything else to lose.", termbox.ColorWhite, termbox.ColorBlack)
}

func (s *GameScreen) ProcessEvent(ev termbox.Event) (done bool, result interface{}) {
	if ev.Type == termbox.EventKey {
		if ev.Key == termbox.KeyArrowUp {
			s.center = s.center.Up()
		} else if ev.Key == termbox.KeyArrowDown {
			s.center = s.center.Down()
		} else if ev.Key == termbox.KeyArrowLeft {
			s.center = s.center.Left()
		} else if ev.Key == termbox.KeyArrowRight {
			s.center = s.center.Right()
		} else if ev.Key == termbox.KeyEnter {
			return true, &NoticeScreen{"Congratulations, you win!", termbox.ColorGreen}
		} else if ev.Ch == 'S' || ev.Ch == 's' {
			s.m = SmoothMap(s.m)
		} else {
			return true, &NoticeScreen{loseMsg, termbox.ColorRed}
		}
	}
	return false, nil
}

type NoticeScreen struct {
	message string
	fg      termbox.Attribute
}

func (s NoticeScreen) Render() {
	clear()
	setString(10, 4, s.message, s.fg, termbox.ColorBlack)
	setString(10, 6, "Press anything to go back to main menu.", termbox.ColorWhite, termbox.ColorBlack)
}

func (s NoticeScreen) ProcessEvent(ev termbox.Event) (done bool, result interface{}) {
	return ev.Type == termbox.EventKey, nil
}
