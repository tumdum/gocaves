package main

import (
	"log"
	"math/rand"
	"os"
	"runtime"
	"time"

	"github.com/nsf/termbox-go"
)

var (
	logger *log.Logger
)

func poolEvents(events chan termbox.Event) {
	for {
		events <- termbox.PollEvent()
	}
}

type Engine struct {
	events  chan termbox.Event
	screens []Screen
}

func NewEngine() *Engine {
	return &Engine{
		events:  make(chan termbox.Event),
		screens: []Screen{},
	}
}

func (e *Engine) start() {
	runtime.GOMAXPROCS(runtime.NumCPU())
	rand.Seed(time.Now().UnixNano())
	f, err := os.Create("gocaves.log")
	if err != nil {
		log.Fatalln(err)
	}
	logger = log.New(f, "", log.LstdFlags|log.Lshortfile|log.Lmicroseconds)
	if err := termbox.Init(); err != nil {
		logger.Fatalln(err)
	}
	termbox.SetInputMode(termbox.InputMouse | termbox.InputEsc)
	go poolEvents(e.events)
	logger.Println("Game started.")
}

func (e *Engine) stop() {
	termbox.Close()
	logger.Println("Game ended.")
}

func (e *Engine) render() {
	for _, screen := range e.screens {
		screen.Render()
	}
	termbox.Flush()
}

func (e *Engine) topScreen() Screen {
	return e.screens[len(e.screens)-1]
}

func (e *Engine) popScreen() {
	e.screens = e.screens[:(len(e.screens) - 1)]
}

func (e *Engine) pushScreen(s Screen) {
	e.screens = append(e.screens, s)
}

func (e *Engine) processEvent(ev termbox.Event) {
	done, result := e.topScreen().ProcessEvent(ev)
	if done {
		e.popScreen()
	}
	switch result := result.(type) {
	case Screen:
		e.pushScreen(result)
	}
}

func (e *Engine) Run(start Screen) {
	e.start()
	defer e.stop()
	e.screens = []Screen{start}

	for len(e.screens) > 0 {
		select {
		case ev := <-e.events:
			e.processEvent(ev)
		case <-time.After((1000 / 30) * time.Millisecond):
		}
		e.render()
	}
}
