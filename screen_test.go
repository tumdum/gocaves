package main

import (
	"testing"

	"github.com/nsf/termbox-go"
)

func TestEnterWinsAtGameScreen(t *testing.T) {
	s := GameScreen{}
	remove, nextScreen := s.ProcessEvent(termbox.Event{
		Type: termbox.EventKey,
		Key:  termbox.KeyEnter,
	})
	isNoticeScreenWithMessage(nextScreen, winMsg, t)
	if !remove {
		t.Fatalf("After wining game screen should be removed, but it's not.")
	}
}

func TestOtherKeysLoseAtGameScreen(t *testing.T) {
	events := []termbox.Event{
		termbox.Event{Type: termbox.EventKey, Key: termbox.KeyEsc},
	}
	for _, event := range events {
		s := GameScreen{}
		remove, nextScreen := s.ProcessEvent(event)
		isNoticeScreenWithMessage(nextScreen, loseMsg, t)
		if !remove {
			t.Fatalf("After losing game screen should be removed, but it's not.")
		}
	}
}

func isNoticeScreenWithMessage(screen interface{}, msg string, t *testing.T) {
	if screen == nil {
		t.Fatalf("Expected win notice screen, got nil")
	}
	noticeScreen, isNoticeScreen := screen.(*NoticeScreen)
	if !isNoticeScreen || noticeScreen.message != msg {
		t.Fatalf("Expected win notice screen, got '%#v'", screen)
	}
}
